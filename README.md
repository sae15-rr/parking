# SAÉ Données - Analyse des données des parkings de Montpellier

## Structure

### Collecteur

Le [collecteur](src/collector/README.md) est chargé de recolter les données depuis les APIs.

### Analyse

L'interprétation des résultats est disponible sous forme d'un [notebook jupyter](src/analysis/jupyter.ipynb)

### Données

Les données brutes récoltées sont disponible via [Git LFS](https://git-lfs.com/) dans le dossier [data/](data/).